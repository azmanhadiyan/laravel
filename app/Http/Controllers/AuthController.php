<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('data.register');
    }
    public function welcome(Request $request){
        $name = $request->first_name;
        $last_name = $request->last_name;

        return view('/data.welcome', compact('name','last_name'));
        
    }
}
