<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
		
		<!-- bagian awal, isi tag heading -->
		<div>
		<div>
			<h1>Buat Account Baru!</h1>
		</div>

		<!-- form -->
		<form action="/welcome" method="POST">
            @csrf
			<h3>Sign Up Form</h3>
			<label for="first_name">First Name :</label><br><br>
			<input type="text" name="first_name">

			<br><br>
			<label for="last_name">Last Name :</label><br><br>
			<input type="text" name="last_name">

			<br><br>
			<label for="gender">Gender :</label><br><br>
			<input type="radio" name="gender" value="0" checked=""> Male <br>
			<input type="radio" name="gender" value="1"> Female <br>
			<input type="radio" name="gender" value="2"> Other <br>

			<br>
			<label>Nationality :</label><br><br>
			<select>
				<optgroup label="Asia">
					<option value="indonesia">Indonesia</option>
					<option value="japan">Japan</option>
					<option value="arab">Arab</option>
				</optgroup>
				<optgroup label="Europa">
					<option value="belgia">Belgia</option>
					<option value="inggris">Inggris</option>
					<option value="spanyol">Spanyol</option>
				</optgroup>
			</select>
			
			<br><br>
			<label for="language">Language Spoken :</label><br><br>
			<input type="checkbox" name="b.indonesia" value="0" checked=""> Bahasa Indonesia <br>
			<input type="checkbox" name="b.inggris" value="1"> English <br>
			<input type="checkbox" name="other" value="2"> Other <br>

			<br>
			<label for="bio">Bio :</label><br>
			<textarea name="bio" cols="22" rows="8"></textarea>

			<br>
			<input type="submit" value="Sign Up" >
		</form>
		
	</body>
</html>